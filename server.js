const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const config = require('./config');

const userCtrl = require('./db/controllers/User');
const orderCtrl = require('./db/controllers/Order');

const api_user = express.Router();

api_user.get('/findAllUsers', userCtrl.findAllUsers);
api_user.post('/getUser', userCtrl.getUser);
api_user.get('/getOrders', orderCtrl.getOrders);
api_user.get('/getOrdersById/:id', orderCtrl.getOrdersById);
//api_user.post('/saveOrder', orderCtrl.saveOrder);





// Evitar problemas de CORS:
app.use(function(req, res, next) {

	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

	// Website you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', '*');

	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);

	// Pass to next layer of middleware
	next();
});

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(express.static(__dirname));
app.use(bodyParser.json());

app.use('/user', api_user);
app.use('/order', api_user);

app.listen(config.port, () => {
    console.log('API REST Corriendo');
});
