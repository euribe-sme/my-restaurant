const config = require('../../config');
// Conexión a la BD:
const conn = require('../index');

function Inserts(template, data) {
    if (!(this instanceof Inserts)) {
        return new Inserts(template, data);
    }
    this._rawDBType = true;
    this.formatDBType = function() {
        return data.map(d => '(' + conn.pgp.as.format(template, d) + ')').join(',');
    };
}


saveOrder = order => {
  return conn.db.one('INSERT INTO public.orders(client, amount, state, date, hour, payment) VALUES ($1, $2, $3, $4, $5, $6)',
                            [order.client, order.amount, order.state, order.date, order.hour, order.payment]);
}

saveSaleOrder = saleOrders => {
  return conn.db.one('INSERT INTO public.saleOrders(idOrder, idDishes) VALUES $1',
                      Inserts('${idorder}, ${iddish}', saleOrders));
}

getOrders = () => {
  return conn.db.many('SELECT a.id, a.client, a.amount, a.state, a.date, a.hour, a.payment, b.name, b.price, b.features ' +
                      	'FROM orders a ' +
                      	'INNER JOIN saleorders c on a.id = c.idorder ' +
                      	'INNER JOIN dishes b on c.iddish = b.id ORDER BY a.id ASC ');
}

getOrdersById = (id) => {
  return conn.db.many('SELECT a.id, a.client, a.amount, a.state, a.date, a.hour, a.payment, b.name, b.price, b.features ' +
                      	'FROM orders a ' +
                      	'INNER JOIN saleorders c on a.id = c.idorder ' +
                      	'INNER JOIN dishes b on c.iddish = b.id WHERE a.id = $1  ORDER BY a.id ASC ', [id]);
}

module.exports = {
  saveOrder,
  saveSaleOrder,
  getOrders,
  getOrdersById
}
