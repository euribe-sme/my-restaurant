const config = require('../../config');
// Conexión a la BD:
const conn = require('../index');

module.exports.getAllUsers = () => {
  return conn.db.any('select * from users');
};

module.exports.getUser = (userM, userP) => {
  return conn.db.any('select * from users where email = $1 and password = $2', [userM, userP]);
};
