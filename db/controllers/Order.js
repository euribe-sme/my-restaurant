const capaDB = require('../models/Order');

saveOrder = async (req, res) => {
  try {
    console.log(req.body.order);
    let order = await capaDB.saveOrder(req.body);
    let result = await capaDB.saveSaleOrder(req.body.order.sales);
    res.status(200).send( { status: 200, orderId: order } );
  } catch (e) {
    res.status(500).send(e);
  }
}

getOrders = (req, res) => {
  capaDB.getOrders()
  .then(data => {
    let arrOrders = [];
    let arrSales = [];
    let indexPrev = 1;
    let myelem = {};
    data.forEach((elem, index) => {
      let objSale = {};

      if(elem.id == indexPrev){
        myelem.id = elem.id;
        myelem.client= elem.client;
        myelem.amount = elem.amount;
        myelem.state = elem.state;
        myelem.date = elem.date;
        myelem.hour = elem.hour;
        myelem.payment = elem.payment;
        objSale.name = elem.name;
        objSale.price = elem.price;
        objSale.features = elem.features;
        arrSales.push(objSale);
      }
      else {
        let obj = {};
        obj.id = myelem.id;
        obj.client= myelem.client;
        obj.amount = myelem.amount;
        obj.state = myelem.state;
        obj.date = myelem.date;
        obj.hour = myelem.hour;
        obj.payment = myelem.payment;
        obj.sales = arrSales;
        arrOrders.push(obj);
        arrSales = [];
        myelem = {};
        indexPrev = elem.id;
        /***************************/
        myelem.id = elem.id;
        myelem.client= elem.client;
        myelem.amount = elem.amount;
        myelem.state = elem.state;
        myelem.date = elem.date;
        myelem.hour = elem.hour;
        myelem.payment = elem.payment;
        objSale.name = elem.name;
        objSale.price = elem.price;
        objSale.features = elem.features;
        arrSales.push(objSale);
      }
    })
    let obj = {};
    obj.id = myelem.id;
    obj.client= myelem.client;
    obj.amount = myelem.amount;
    obj.state = myelem.state;
    obj.date = myelem.date;
    obj.hour = myelem.hour;
    obj.payment = myelem.payment;
    obj.sales = arrSales;
    arrOrders.push(obj);

    res.status(200).send( { status: 200, data: arrOrders } );
  })
  .catch( function(err) {
    res.status(500).send(err);
  });
}

getOrdersById = (req, res) => {
  capaDB.getOrdersById(req.params.id)
  .then(data => {
    let arrOrders = [];
    let arrSales = [];
    let indexPrev = req.params.id;
    let myelem = {};
    data.forEach((elem, index) => {
      let objSale = {};

      if(elem.id == indexPrev){
        myelem.id = elem.id;
        myelem.client= elem.client;
        myelem.amount = elem.amount;
        myelem.state = elem.state;
        myelem.date = elem.date;
        myelem.hour = elem.hour;
        myelem.payment = elem.payment;
        objSale.name = elem.name;
        objSale.price = elem.price;
        objSale.features = elem.features;
        arrSales.push(objSale);
      }
      else {
        let obj = {};
        obj.id = myelem.id;
        obj.client= myelem.client;
        obj.amount = myelem.amount;
        obj.state = myelem.state;
        obj.date = myelem.date;
        obj.hour = myelem.hour;
        obj.payment = myelem.payment;
        obj.sales = arrSales;
        arrOrders.push(obj);
        arrSales = [];
        myelem = {};
        indexPrev = elem.id;
        /***************************/
        myelem.id = elem.id;
        myelem.client= elem.client;
        myelem.amount = elem.amount;
        myelem.state = elem.state;
        myelem.date = elem.date;
        myelem.hour = elem.hour;
        myelem.payment = elem.payment;
        objSale.name = elem.name;
        objSale.price = elem.price;
        objSale.features = elem.features;
        arrSales.push(objSale);
      }
    })
    let obj = {};
    obj.id = myelem.id;
    obj.client= myelem.client;
    obj.amount = myelem.amount;
    obj.state = myelem.state;
    obj.date = myelem.date;
    obj.hour = myelem.hour;
    obj.payment = myelem.payment;
    obj.sales = arrSales;
    arrOrders.push(obj);

    res.status(200).send( { status: 200, data: arrOrders } );
  })
  .catch( function(err) {
    res.status(500).send(err);
  });
}
module.exports = {
  saveOrder,
  getOrders,
  getOrdersById
}
