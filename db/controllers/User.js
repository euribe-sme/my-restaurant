const capaDB = require('../models/User');

findAllUsers = (req, res) => {
  capaDB.getAllUsers()
  .then(data => {
    res.status(200).send( { status: 200, data: data } );
  })
  .catch( function(err) {
    res.status(500).send(err);
  });
}

getUser = (req, res) => {
  console.log(req.body.email, req.body.psw);
  capaDB.getUser(req.body.email, req.body.psw)
  .then(data => {
      console.log(data);
      res.status(200).send( { status: 200, data: data } );
  })
  .catch( function(err) {
    res.status(500).send(err);
  });
}
module.exports = {
    findAllUsers,
    getUser
}
