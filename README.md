# Prueba Tekton Labs

Aplicación Mi Restaurant para registrar ordenes de platos.  
* Front: React
* Backend: node
* Framework diseño: Materialize
* Base de datos: PostgreSQL

## Para correr servidor REST

```
npm start
```
## Para correr app cliente

```
cd client
npm start
```
