import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import Home from './pages/home';
import SignIn from './pages/signin';
import Orders from './pages/orders';
import Order from './pages/order';
import CreateOrder from './pages/createOrder';
import About from './pages/about';
import Posts from './pages/posts';
import Post from './pages/post';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Route exact path="/" component={Home} />
          <Route exact path="/signin" component={SignIn} />
          <Route exact path="/orders" component={Orders} />
          <Route exact path="/order/:orderId" component={Order} />
          <Route exact path="/order" component={CreateOrder} />
          <Route path="/about" component={About} />
          <Route path="/posts" component={Posts} />
          <Route path="/post/:postId" component={Post} />
        </div>
      </Router>
    );
  }
}

export default App;
