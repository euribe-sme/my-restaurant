import React, { Component } from 'react';

class SignIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };

     this.handleChange = this.handleChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleChange(event) {
    if(event.target.name == 'email') {
      this.setState({
        email: event.target.value,
      });
    }
    else {
      this.setState({
        password: event.target.value
      });
    }
  }

  handleSubmit(event){
    event.preventDefault();
    let email = this.state.email;
    let pwd = this.state.password;
    console.log(email, pwd);
    let data = {
      email: this.state.email,
      psw: this.state.password
    }

      var xmlHttp = new XMLHttpRequest();
      xmlHttp.onreadystatechange = (e) => {
        if (xmlHttp.readyState !== 4) {
          return;
        }

        if (xmlHttp.status === 200) {
          if(JSON.parse(xmlHttp.responseText).status == 200)
          {
            console.log(JSON.parse(xmlHttp.responseText).data[0]);
            window.location.href = '/orders'; 
          }
        } else {
          console.warn('error');
        }
      };
      xmlHttp.open("POST", 'http://localhost:3001/user/getUser'); // false for synchronous request
      xmlHttp.setRequestHeader("Content-type", "application/json");
      xmlHttp.send(JSON.stringify(data));

  }

  render() {
    return (
      <div className="col s12 m7">
        <div className="row">
          <div className="signup-box">
            <h1>Bienvenido</h1>
            <form className="signup-form" onSubmit={this.handleSubmit}>


                <input type="text" name="email" placeholder="Correo" value={this.state.email} onChange={this.handleChange} />
                <input type="password" name="password" placeholder="Contraseña" value={this.state.password} onChange={this.handleChange} />
                <button className="btn waves-effect waves-light btn-signup" type="submit">Iniciar Sesion</button>

            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default SignIn;
