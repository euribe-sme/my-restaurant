import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import gOrders from '../data/orders';
import Menu from '../components/menu';

//import react-materialize from 'react-materialize'

class Orders extends Component {
  state = { ordenes: [] }

  componentWillMount() {
    fetch('http://localhost:3001/order/getOrders')
      .then((response) => {
        return response.json()
      })
      .then((ordenes) => {
        this.setState({ ordenes: ordenes.data })
      })
  }

  onOrderClick(order) {
    //this.props.params.order = order;
    window.location.href = `/order/${order.id}`;
  }

  render() {

    return (
      <section className="orders">
        <Menu />
        <div className="row">
          <div>
            <h3>Listado de ordenes</h3>
          </div>
          <div>
            <a className="waves-effect waves-light btn modal-trigger" href='/order'><i className="material-icons">add</i></a>

          </div>
        </div>
          <table className="striped">
            <thead>
              <tr>
                  <th>Codigo</th>
                  <th>Cliente</th>
                  <th>Monto</th>
                  <th>Estado</th>
              </tr>
            </thead>

            <tbody>
            {this.state.ordenes.map(order =>
              <tr key={order.id} className="order" onClick={() => this.onOrderClick(order)}>
                <td>
                {order.id}
                </td>
                <td>
                {order.client}
                </td>
                <td>
                {order.amount} $
                </td>
                <td>
                {(order.state) ? <div><input type="checkbox" id="test5" defaultChecked  /><label htmlFor="test5"></label></div> : <div><input type="checkbox" id="test6" /> <label htmlFor="test6"></label></div>  }
                </td>
              </tr>
            )}

            </tbody>
          </table>

      </section>
    )
  }
}

export default Orders;
