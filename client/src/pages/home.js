import React from 'react';
import Menu from '../components/menu';

const Home = () => (
  <section>
    <Menu />
    <h3>Sobre Mi Restaurant</h3>
    <p>Comer es uno de los placeres en la vida y con Mi Restaurant hacemos mas placentero tu degustación</p>
  </section>
);

export default Home;
