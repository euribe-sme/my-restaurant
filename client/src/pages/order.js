import React, { Component } from 'react';
import orders from '../data/orders';
import Menu from '../components/menu';
import OrderComp from './orders';

class Order extends Component {
  state = { orden: [] }

  componentWillMount() {
    const { match } = this.props;
    fetch(`http://localhost:3001/order/getOrdersById/${match.params.orderId}`)
      .then((response) => {
        return response.json()
      })
      .then((orden) => {
        console.log(orden)
        this.setState({ orden: orden.data[0] })
      })
  }

  render() {
    console.log(this.state.orden.sales);
    return (
      <section>
        <Menu />
        <div className="col s12 m7">
          <div className="row">
            <div className="signup-box">
              <h2>Detalles de orden</h2>
              <form className="signup-form">
                <div className="divider"></div>
                <div className="section">
                  <div className="input-field col s6">
                    <i className="material-icons prefix">account_circle</i>
                    <input id="icon_prefix" type="text" className="validate" value={this.state.orden.client}/>
                    <label htmlFor="icon_prefix">Nombre cliente</label>
                  </div>
                  <div className="input-field col s6">
                    <i className="material-icons prefix">attach_money</i>
                    <input id="icon_prefix" type="text" className="validate" value={this.state.orden.amount}/>
                    <label htmlFor="icon_prefix">Monto a pagar</label>
                  </div>

                   <select className="browser-default ">
                     <option value="" disabled defaultValue>Tipo de pago</option>
                     {(this.state.orden.payment == 'Efectivo') ? <option value="2"  defaultValue >Efectivo</option> : <option value="1" defaultValue >Transferencia</option>}

                   </select>

                </div>
              </form>
              <div className="input-field">
                 <input id="search" type="search" required placeholder="Buscar plato" />
                 <label className="label-icon" htmlFor="search"><i className="material-icons">search</i></label>
                 <i className="material-icons">close</i>
             </div>
             <ul className="collapsible" data-collapsible="accordion">
             {(this.state.orden.sales!==undefined ) && this.state.orden.sales.map(elem =>
               <li>
                 <div className="collapsible-header">
                   {elem.name}
                   <span className="badge">{elem.price}$</span></div>
                 <div className="collapsible-body"><p>{elem.features}.</p></div>
               </li>

             )}
              </ul>
            </div>
          </div>
        </div>
      </section>


    )
  }
}
export default Order;
/*
<div className="order-section">
<h2>{order.client}</h2>
<p>{order.total}</p>
</div>*/
