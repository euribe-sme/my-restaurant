import React, { Component } from 'react';
import Menu from '../components/menu';

class CreateOrder extends Component {
  constructor(props) {
    super(props);
    window.onload = function() {
      //document.getElementById('myselect').material_select();
    }
  }
  render() {

    return (
      <section>
        <Menu />
        <div className="col s12 m7">
          <div className="row">
            <div className="signup-box">
              <h2>Nueva orden</h2>
              <form className="signup-form">
                <div className="divider"></div>
                <div className="section">
                  <div className="input-field col s6">
                    <i className="material-icons prefix">account_circle</i>
                    <input id="icon_prefix" type="text" className="validate"/>
                    <label htmlFor="icon_prefix">Nombre cliente</label>
                  </div>
                  <div className="input-field col s6">
                    <i className="material-icons prefix">attach_money</i>
                    <input id="icon_prefix" type="text" className="validate"/>
                    <label htmlFor="icon_prefix">Monto a pagar</label>
                  </div>
                   <select className="browser-default ">
                     <option value="" disabled defaultValue>Tipo de pago</option>
                     <option value="1">Transferencia</option>
                     <option value="2">Efectivo</option>
                   </select>

                </div>
              </form>
              <div className="input-field">
                 <input id="search" type="search" required placeholder="Buscar plato" />
                 <label className="label-icon" htmlFor="search"><i className="material-icons">search</i></label>
                 <i className="material-icons">close</i>
             </div>
             <ul className="collapsible" data-collapsible="accordion">
                <li>
                  <div className="collapsible-header">
                    Primer plato
                    <span className="badge">4</span></div>
                  <div className="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
                </li>
                <li>
                  <div className="collapsible-header">
                    Segundo Plato
                    <span className="badge">1</span></div>
                  <div className="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>


    )
  }
}
export default CreateOrder;

/*
<button className="btn waves-effect waves-light btn-signup" type="submit">Crear</button>
*/
