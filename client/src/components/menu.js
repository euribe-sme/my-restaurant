import React from 'react';
import { Link } from 'react-router-dom';

const Menu = () => (
  <nav>
    <ul>
      <li><a href="/">Inicio</a></li>
      <li><a href="/signin">Iniciar sesión</a></li>
    </ul>
  </nav>
)

export default Menu;
