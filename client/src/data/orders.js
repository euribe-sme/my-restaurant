import faker from 'faker';

let orders = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11, 12,13, 14];

orders = orders.map(order => {
  return {
    id: order,
    client: faker.name.findName(),
    amount: faker.finance.amount(),
    state: 1
  }
});

function getListOrders() {
  /*var settings = {
				async: true,
				crossDomain: true,
				url: 'http://localhost:3001/order/getOrders',
				method: "GET",
				success: function(res) {
					return res;
					//that.byId("listCalendars").setSelectedItem(that.byId("listCalendars").getItems()[0], true);
					//that.findItemInList();
				},
				error: function(err) {
					console.log(err);
				}
			};
			$.ajax(settings);*/

  return fetch('http://localhost:3001/order/getOrders', {
    accept: "application/json"
  })
  .then(response => {
      //console.log(response);
      return response;
    })
    .then(response => response.json())
    .then(parsedData => {
      console.log(parsedData);
      return parsedData;
    });

}

const gOrders = { orders, getListOrders };
export default gOrders;
